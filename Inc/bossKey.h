#ifndef BOSSKEY_H_
#define BOSSKEY_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "joystick.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"
#include "ansi.h"

typedef struct{
	uint8_t boss, clear;
}bossy;

void bossKey(bossy *key1);

#endif /* BOSSKEY_H_ */
