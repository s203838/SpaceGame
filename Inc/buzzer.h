
#ifndef BUZZER_H_
#define BUZZER_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"
#include "timers.h"

typedef struct{
	uint16_t note, enable, on;
}music;

void initBuzzer();
void playShot(music *mus);
void playFurElize(music *mus);
void playSweetHome(music *mus);
void setFreq(uint16_t freq);

#endif /* BUZZER_H_ */
