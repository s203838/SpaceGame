#pragma once

#include "objects.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"

#define ESC 0x1B

void deathScreen();
void showHelpMenu();
void showMainMenu();
uint32_t nextStage(uint32_t stage, AstroidNode ** head);

