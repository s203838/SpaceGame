#include "objects.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"
#include "timers.h"
#include "gameState.h"

extern uint32_t highScore;
extern uint8_t lvlFlag;
extern uint32_t drawFlag;

void gravity(AstroidNode * astroid, BulletNode * bullet);
void checkColision(Player * player, DockingStation ds,
		AstroidNode** astroidList, BulletNode** bulletList);
