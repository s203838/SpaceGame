#pragma once

#include "charset.h"
#include "objects.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"

void led_init();
void led_life(Player *player);
void init_lcd(uint8_t * buffer);
void lcd_write_string(uint8_t* buffer, char* string, uint8_t line, uint8_t offset);
