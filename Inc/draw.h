#include "objects.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"
#include "timers.h"

extern uint32_t drawFlag;

#define ESC 0x1B

// These functions draws the objects using printf,

void drawPlayer(Player * player);
void drawAstroid(AstroidNode* astroid);
void drawBullet(BulletNode* bullet);
void drawDockingStation(DockingStation * ds);
void unDrawDocking(DockingStation ds);
