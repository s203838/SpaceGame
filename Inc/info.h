#include "objects.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"
#include "led_lcd.h"

/* These functions write info to the LCD display,
 * using a predefined buffer
 */

void writeHighScore(uint8_t * buffer, uint32_t highScore);
void writeLife(uint8_t * buffer, Player * player);
void writeLvl(uint8_t * buffer, Player * player);
void writeStage(uint8_t * buffer, uint32_t stagen);
