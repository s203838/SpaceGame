#ifndef TIMERS_H_
#define TIMERS_H_
#include <stdint.h>
#include <stdio.h>

typedef struct {
	uint16_t t2, t15, t16;
} tim;

void timer15Setup();
void timer16Setup();

void timerInit();

uint16_t getTimer15();
uint16_t getTimer16();

#endif /* TIMERS_H_ */
