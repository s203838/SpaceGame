/*
 * joystick.h
 *
 *  Created on: 8. jun. 2021
 *      Author: tjasm
 */

#ifndef JOYSTICK_H_
#define JOYSTICK_H_
uint16_t readJoystick();
void DTB(uint16_t number);
void setLed(uint16_t n);


#endif /* JOYSTICK_H_ */
