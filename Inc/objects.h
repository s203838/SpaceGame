#ifndef OBJECTS_H_
#define OBJECTS_H_

#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"

typedef struct {
	int32_t posX, posY, velX, velY, enable, lvl, life;
} Player;

typedef struct {
	int32_t posX, posY, velX, velY, enable;
	struct AstroidNode* next;
} AstroidNode;

typedef struct {
	int32_t posX, posY, velX, velY, enable;
	struct BulletNode* next;
} BulletNode;

typedef struct {
	int32_t posX, posY, enable;
} DockingStation;

void addBullet(BulletNode ** head, int32_t posX, int32_t posY,
		int32_t velX, int32_t velY);
void removeBullet(BulletNode * bullet, BulletNode ** head);
void addAstroid(AstroidNode ** head, int32_t posX, int32_t posY,
		int32_t velX, int32_t velY);
void removeAstroid(AstroidNode * astroid, AstroidNode ** head);
Player playerInit(Player * player);

#endif
