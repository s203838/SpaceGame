#include "info.h"

void writeHighScore(uint8_t * buffer, uint32_t highScore) {
	char newScore[9];
	newScore[8] = '\0';
	// Converts to int to char
	itoa(highScore, newScore, 10);
	lcd_write_string(buffer, newScore ,0,40);
	lcd_push_buffer(buffer);
}

void writeLife(uint8_t * buffer, Player * player) {
	char newLife[2];
	newLife[1] = '\0';
	// Converts to int to char
	itoa(player->life, newLife, 10);
	lcd_write_string(buffer, newLife, 1, 40);
	lcd_push_buffer(buffer);
}

void writeLvl(uint8_t * buffer, Player * player) {
	char newLvl[2];
	newLvl[1] = '\0';
	// Converts to int to char
	itoa(player->lvl, newLvl, 10);
	lcd_write_string(buffer, newLvl, 2, 40);
	lcd_push_buffer(buffer);
}

void writeStage(uint8_t * buffer, uint32_t stagen) {
	char stage[2];
	stage[1] = '\0';
	// Converts to int to char
	itoa(stagen, stage, 10);
	lcd_write_string(buffer, stage, 3, 40);
	lcd_push_buffer(buffer);
}
