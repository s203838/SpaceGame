#include "col_grav.h"

void gravity(AstroidNode * astroid, BulletNode * bullet) {
			// If ball is just above the asteroid, velocity gets set 1 higher in the y-direction
			if (astroid->posX + 5 < bullet->posX && astroid->posX + 10 > bullet->posX
					&& astroid->posY > bullet->posY && astroid->posY < bullet->posY + 3){
				bullet->velY = 1;
				bullet->velX = 2;
			}
			// If ball is just below the asteroid, velocity gets set 1 lower in the y-direction
			if (astroid->posX + 5 < bullet->posX && astroid->posX + 10 > bullet->posX
					&& astroid->posY + 5 < bullet->posY && astroid->posY + 9 > bullet->posY){
				bullet->velY = -1;
				bullet->velX = 2;
		}
}

void checkColision(Player * player, DockingStation ds, AstroidNode** astroidList, BulletNode** bulletList) {
	// Checks if its docking level
	if (lvlFlag & (0x0001)) {
		if((ds.posX <= player->posX ) && (ds.posX+5 >= player->posX )) {
			if((ds.posY <= player->posY ) && (ds.posY+4 >= player->posY )) {
					player->lvl++;
					lvlFlag &= ~0x0001;
					unDrawDocking(ds);
			}
		}
		return;
	}

	// Start of loop that goes throug all objects
	AstroidNode * astroid = *astroidList;
	while(astroid != NULL) {

		BulletNode * bullet = *bulletList;
		while(bullet != NULL) {

			// Calls gravity on all objects
			gravity(astroid, bullet);

			// Checks if bullets hits asteroid
			if ((astroid->posX <= bullet->posX ) &&
					((astroid->posX+5 >= bullet->posX ))) {
				if ((astroid->posY <= bullet->posY ) &&
					((astroid->posY+4 >= bullet->posY ))) {
					removeAstroid(astroid, astroidList);
					removeBullet(bullet, bulletList);
					highScore += 500;
					drawFlag |= (0x0001 << 3);
				}
			}
			bullet = bullet->next;
		}
		// Checks if player hits asteroid
		if ((astroid->posX <= player->posX ) &&
				((astroid->posX+5 >= player->posX ))) {
			if ((astroid->posY <= player->posY ) &&
				((astroid->posY+4 >= player->posY ))) {
				removeAstroid(astroid, astroidList);
				player->life--;
				drawFlag |= (0x0001 << 3);
			}
		}
		// Goes to next asteroid in list
		astroid = astroid->next;
	}
}
