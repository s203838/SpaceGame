#pragma once
#include "objects.h"
#define ESC 0x1B

void addBullet(BulletNode ** head, int32_t posX, int32_t posY, int32_t velX, int32_t velY) {
	BulletNode * bullet = *head;
	if (*head == NULL) {
		BulletNode * temp = (BulletNode *)malloc(sizeof(BulletNode));
		temp->next = NULL;
		temp->posX = posX;
		temp->posY = posY;
		temp->velX = velX;
		temp->velY = velY;
		*head = temp;
		return;
	}
	while(1) {
		if (bullet->next == NULL ) {
			BulletNode * temp = (BulletNode *)malloc(sizeof(BulletNode));
			temp->next = NULL;
			temp->posX = posX;
			temp->posY = posY;
			temp->velX = velX;
			temp->velY = velY;
			bullet->next = temp;
			break;
		}
		bullet = bullet->next;
	}
}

void removeBullet(BulletNode * bullet, BulletNode ** head) {
	BulletNode * loopBullet = *head;
	if ((*head == bullet) && (bullet->next == NULL)) {
		*head = NULL;
		gotoxy(bullet->posY, bullet->posX);
		printf(" ");
		free(bullet);
		return;
	}
	if (*head == bullet) {
		*head = bullet->next;
		gotoxy(bullet->posY, bullet->posX);
		printf(" ");
		free(bullet);
		return;
	}
	while(loopBullet->next != bullet) {
		loopBullet = loopBullet->next;
	}
	gotoxy(bullet->posY, bullet->posX);
	printf(" ");
	loopBullet->next = bullet->next;
	free(bullet);
}

void addAstroid(AstroidNode ** head, int32_t posX, int32_t posY,
		int32_t velX, int32_t velY) {
	AstroidNode * astroid = *head;
	if (*head == NULL) {
		AstroidNode * temp = (AstroidNode *)malloc(sizeof(AstroidNode ));
		temp->next = NULL;
		temp->posX = posX;
		temp->posY = posY;
		temp->velX = velX;
		temp->velY = velY;
		*head = temp;
		return;
	}
	while(1) {
		if (astroid->next == NULL ) {
			AstroidNode * temp = (AstroidNode *)malloc(sizeof(AstroidNode ));
			temp->next = NULL;
			temp->posX = posX;
			temp->posY = posY;
			temp->velX = velX;
			temp->velY = velY;
			astroid->next = temp;
			break;
		}
		astroid = astroid->next;
	}
}

void removeAstroid(AstroidNode * astroid, AstroidNode ** head) {
	AstroidNode * loopAstroid = *head;
	if (*head == astroid && astroid->next == NULL) {
		*head = NULL;
		gotoxy(astroid->posY, astroid->posX);
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 9, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		free(astroid);
		return;
	}
	if (*head == astroid) {
		*head = astroid->next;
		gotoxy(astroid->posY, astroid->posX);
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 9, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		printf("%c[%d%c", ESC, 1, 'B');
		printf("%c[%d%c", ESC, 8, 'D');
		printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
		free(astroid);
		return;
	}
	while(loopAstroid->next != astroid) {
		if (loopAstroid == NULL) {
			return;
		}
		loopAstroid = loopAstroid->next;
	}
	gotoxy(astroid->posY, astroid->posX);
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 9, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	loopAstroid->next = astroid->next;
	if (loopAstroid != NULL) {
		free(astroid);
	}
}

Player playerInit(Player * player) {
	player->posX = 5;
	player->posY = 5;
	player->velX = 0;
	player->velY = 0;
	player->life = 3;
	player->lvl = 0;
}
