#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h" 		// Input/output library for this course
#include "ansi.h"
#include "charset.h"
#include "timers.h"
#include "bossKey.h"
#include "buzzer.h"
#include <stdio.h>
#include <string.h>
#include "objects.h"
#include "joyStick.h"
#include "info.h"
#include "draw.h"
#include "gameState.h"
#include "col_grav.h"

#define ESC 0x1B

extern uint32_t drawFlag;

int32_t randInt(int32_t n) {
	return rand() % n;
}

int main(void)
{
	// Setup of the game

	uint32_t stage = 1;
	AstroidNode* astroidList = NULL;
	BulletNode* bulletList = NULL;
	Player player;
	DockingStation station;
	station.posX = 50;
	station.posY = 50;
	playerInit(&player);
	uint8_t * buffer = malloc(512);
	memset(buffer,0,512);
	init_lcd(buffer);
	lcd_push_buffer(buffer);
	led_init();
	timer15Setup();
	timer17Setup();
	initBuzzer();
	initJoyStick();
	bossy key1;

	uart_init(500000);
	printf("%c[2J", 0x1B);

	while(1) {
		// Checks for bosskey
		bossKey(&key1);

		// Checks if there is no more asteroids and then moves to next stage
		if ((astroidList == NULL) && !(lvlFlag & 0x001)) {
			stage = nextStage(stage, &astroidList);
		}

		// Checks if player died
		if (player.life < 1) {
			stage = 0;
			stage = nextStage(stage, &astroidList);
		}

		// Checks for input
		char n = uart_get_char();
		if (n == 'w' && player.velY != -1) {
			player.velY--;
			uart_clear();
		}
		if (n == 'a' && player.velX != -2) {
			player.velX--;
			uart_clear();
		}
		if (n == 'd' && player.velX != 2) {
			player.velX++;
			uart_clear();
		}
		if (n == 's' && player.velY != 1) {
			player.velY++;
			uart_clear();
		}
		if (n == 'j') {
			// Plays shooting sound
			setFreq(440);

			// Shoots bullet based on level
			if (player.lvl == 0) {
				addBullet(&bulletList, player.posX+6, player.posY+3, 2 ,0);
			}
			if (player.lvl == 1) {
				addBullet(&bulletList, player.posX+6, player.posY+1, 2 ,0);
				addBullet(&bulletList, player.posX+6, player.posY+4, 2 ,0);
			}
			if (player.lvl == 2) {
				addBullet(&bulletList, player.posX+6, player.posY+3, 2 ,-1);
				addBullet(&bulletList, player.posX+6, player.posY+3, 2 ,0);
				addBullet(&bulletList, player.posX+6, player.posY+3, 2 ,1);
			}
			uart_clear();
		}

		// Stops the shooting sound
		if (n != 'j' && drawFlag & 0x0001){
			setFreq(0);
		}

		// Draws bullets on timer
		if (drawFlag & 0x0001) {
			drawPlayer(&player);
			BulletNode * bullet = bulletList;
			// Loops through all bullets
			while(bullet != NULL) {
				drawBullet(bullet);
				if (bullet->posX > 250) {
					removeBullet(bullet, &bulletList);
				}
				bullet = bullet->next;
			}
			// Resets drawFlag when draw is done
			drawFlag &= ~0x0001;
		}

		// Draws asteroid on timer
		if(drawFlag & 0x0002) {
			AstroidNode * astroid = astroidList;
			// Loops through all bullets
			while(astroid != NULL) {
				drawAstroid(astroid);
				if (astroid->posX == 1) {
					removeAstroid(astroid, &astroidList);
				}
				astroid = astroid->next;
			}

			// Checks if it is a docking stage
			if (lvlFlag & 0x0001) {
				drawDockingStation(&station);
			}
			// Resets drawFlag when draw is done
			drawFlag &= ~0x0002;
		}

		// Checks colision and gravity if something has moved;
		if (drawFlag | 0x0003)
			checkColision(&player, station, &astroidList, &bulletList);

		//Writes info if there is a change in info
		if (drawFlag & 0x0001 << 3) {
			writeLvl(buffer, &player);
			writeLife(buffer, &player);
			writeHighScore(buffer, highScore);
			writeStage(buffer, stage);
			led_life(&player);
			drawFlag &= ~(0x0001 << 3);
		}
	}

}
