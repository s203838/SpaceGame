#include "bossKey.h"

void bossKey(bossy *key1){
	// Initializes boss key when joystick is high
	if(readJoystick() == 1){
		key1->boss = 1;
	}
	while(key1->boss == 1){
		if(key1->clear == 1){
			clrscr();
			gotoxy(20, 52);
			printf("Downloading business stuff...");
			gotoxy(22, 52);
			printf("99%% done");
			key1->clear = 0;
		}
		// If joystick is pressed down everything is cleared and the loop is broken
		if(readJoystick() == 2){
			key1->boss = 0;
			key1->clear = 1;
			clrscr();
		}
	}
}


