#include "gameState.h"

uint32_t highScore = 0;
uint8_t lvlFlag = 0;

void deathScreen() {
	printf("%c[2J", ESC);
	gotoxy(10, 40);
	printf("Game Over");
	gotoxy(12, 40);
	printf("Yous score was %u", highScore);


	// Waits for input to start game over
	while(1) {
		char n = uart_get_char();
		if (n != '\0') {
			uart_clear();
			break;
		}
	}
}

void showHelpMenu() {
	printf("%c[2J", ESC);
	gotoxy(10,40);
	printf("Movement is on the WASD keys and to shoot press J");
	gotoxy(12,40);
	printf("Survive as long as possible in deep space, with Asteroids moving towards you");
	gotoxy(14,40);
	printf("If you shoot an Asteroid you gain 500 points");

	// Waits for input to start game
	while(1) {
		char n = uart_get_char();
		if (n != '\0') {
			uart_clear();
			break;
		}
	}
}

void showMainMenu() {
	printf("%c[2J", ESC);
	gotoxy(10,40);
	printf(R"(
   _____
  / ___/    ____   ____ _  _____  ___           ____ _  ____ _   ____ ___   ___
  \__ \    / __ \ / __ `/ / ___/ / _ \         / __ `/ / __ `/  / __ `__ \ / _ \
 ___/ /   / /_/ // /_/ / / /__  /  __/        / /_/ / / /_/ /  / / / / / //  __/
/____/   / .___/ \__,_/  \___/  \___/         \__, /  \__,_/  /_/ /_/ /_/ \___/
        /_/                                  /____/
)");
	gotoxy(10,20);
	printf("press H for help, or any other key to start");

	// Waits for input
	while(1) {
		char n = uart_get_char();
		if (n == 'h') {
			showHelpMenu();
			break;
		}
		if (n != '\0') {
			uart_clear();
			printf("%c[2J", ESC);
			break;
		}
	}
}

uint32_t nextStage(uint32_t stage, AstroidNode ** head) {

	// Checks if stage is a predetermined

	if(stage == 0) {
		deathScreen();
	}

	if(stage == 1) {
		// Set start speed of asteroids
		timer16Setup(0x00CF);
		showMainMenu();
	}

	if(stage == 2) {
		printf("%c[2J", ESC);
		addAstroid(head,200,20,-1,0);
		addAstroid(head,120,10,-1,0);
		addAstroid(head,180,15,-1,0);
	}

	if(stage == 3) {
		printf("%c[2J", ESC);
		addAstroid(head,200,22,-1,0);
		addAstroid(head,250,12,-1,0);
		addAstroid(head,120,8,-1,0);
		addAstroid(head,230,5,-1,0);
		addAstroid(head,180,17,-1,0);
	}

	if(stage == 4) {
		// Increases the speed of asteroids
		timer16Setup(0x008F);
		printf("%c[2J", ESC);
		// Set level flag for docking stage
		lvlFlag |= 0x0001;
	}

	if(stage == 5) {
		printf("%c[2J", ESC);
		addAstroid(head,200,22,-1,0);
		addAstroid(head,250,12,-1,0);
		addAstroid(head,120,8,-1,0);
		addAstroid(head,230,5,-1,0);
		addAstroid(head,180,17,-1,0);
	}

	if(stage == 6) {
		printf("%c[2J", ESC);
		addAstroid(head,200,40,-1,0);
		addAstroid(head,250,32,-1,0);
		addAstroid(head,120,50,-1,0);
		addAstroid(head,230,100,-1,0);
		addAstroid(head,180,67,-1,0);
	}

	if(stage == 7) {
		// Increases the speed of asteroids
		timer16Setup(0x004F);
		printf("%c[2J", ESC);
		// Set level flag for docking stage
		lvlFlag |= 0x0001;
	}

	if (stage == 8) {
		printf("%c[2J", ESC);
		addAstroid(head,200,22,-1,0);
		addAstroid(head,250,12,-1,0);
		addAstroid(head,120,8,-1,0);
		addAstroid(head,230,5,-1,0);
		addAstroid(head,180,17,-1,0);
		addAstroid(head,215,40,-1,0);
		addAstroid(head,232,32,-1,0);
		addAstroid(head,122,50,-1,0);

	}

	if (stage == 9) {
		printf("%c[2J", ESC);
		addAstroid(head,200,22,-1,0);
		addAstroid(head,250,12,-1,0);
		addAstroid(head,120,8,-1,0);
		addAstroid(head,230,5,-1,0);
		addAstroid(head,180,17,-1,0);
		addAstroid(head,215,40,-1,0);
		addAstroid(head,156,100,-1,0);
		addAstroid(head,96,67,-1,0);
	}

	// Checks if stage is outside of the predetermined ones
	if (stage > 10) {
		printf("%c[2J", ESC);
		// Creates random assignments of asteroids
		addAstroid(head,180+randInt(30),randInt(80),-1,0);
		addAstroid(head,200+randInt(30),randInt(80),-1,0);
		addAstroid(head,220+randInt(30),randInt(80),-1,0);
		addAstroid(head,150+randInt(40),randInt(80),-1,0);
		addAstroid(head,200+randInt(40),randInt(80),-1,0);
		addAstroid(head,220+randInt(40),randInt(80),-1,0);
	}
	return stage+1;
}
