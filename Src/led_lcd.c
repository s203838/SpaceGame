#include "led_lcd.h"

void led_init() {
	RCC->AHBENR |= RCC_AHBPeriph_GPIOA; // Enable clock for GPIO Port A,B & C
	RCC->AHBENR |= RCC_AHBPeriph_GPIOB;
	RCC->AHBENR |= RCC_AHBPeriph_GPIOC;
	//initialize LED pins(A9, B4, C7) for output
	GPIOA->OSPEEDR &= ~(0x00000003 << (9 * 2)); // Clear speed register
	GPIOA->OSPEEDR |= (0x00000002 << (9 * 2)); // set speed register (0x01 - 10MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
	GPIOA->OTYPER &= ~(0x0001 << (9 * 1)); // Clear output type register
	GPIOA->OTYPER |= (0x0000 << (9)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
	GPIOA->MODER &= ~(0x00000003 << (9 * 2)); // Clear mode register
	GPIOA->MODER |= (0x00000001 << (9 * 2)); // Set mode register (0x00 –Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)

	GPIOB->OSPEEDR &= ~(0x00000003 << (4 * 2)); // Clear speed register
	GPIOB->OSPEEDR |= (0x00000002 << (4 * 2)); // set speed register (0x01 - 10MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)							GPIOB->OTYPER &= ~(0x0001 << (4 * 1)); // Clear output type register
	GPIOB->OTYPER |= (0x0000 << (4)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
	GPIOB->MODER &= ~(0x00000003 << (4 * 2)); // Clear mode register
	GPIOB->MODER |= (0x00000001 << (4 * 2)); // Set mode register (0x00 –Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)

	GPIOC->OSPEEDR &= ~(0x00000003 << (7 * 2)); // Clear speed register
	GPIOC->OSPEEDR |= (0x00000002 << (7 * 2)); // set speed register (0x01 - 10MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
	GPIOC->OTYPER &= ~(0x0001 << (7 * 1)); // Clear output type register
	GPIOC->OTYPER |= (0x0000 << (7)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
	GPIOC->MODER &= ~(0x00000003 << (7 * 2)); // Clear mode register
	GPIOC->MODER |= (0x00000001 << (7 * 2)); // Set mode register (0x00 –Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
}

void led_life(Player *player){

		if(player ->life ==1){
			GPIOA->ODR |= (0x0001 << 9); //Set pin PA9 to high
			GPIOC->ODR |= (0x0001 << 7); //Set pin PC7 to high
			GPIOB->ODR &= ~(0x0001 << 4); //Set pin PB4 to low
		}
		if(player ->life ==2){
			GPIOA->ODR |= (0x0001 << 9); //Set pin PA9 to high
			GPIOC->ODR &= ~(0x0001 << 7); //Set pin PC7 to low
			GPIOB->ODR &= ~(0x0001 << 4); //Set pin PB4 to low){
		}
		if(player ->life ==3){
			GPIOA->ODR |= (0x0001 << 9); //Set pin PA9 to high
			GPIOB->ODR |= (0x0001 << 4); //Set pin PB4 to high
			GPIOC->ODR &= ~(0x0001 << 7); //Set pin PC7 to low
		}
		if(player ->life ==0){
			GPIOA->ODR |= (0x0001 << 9); //Set pin PA9 to high
			GPIOB->ODR |= (0x0001 << 4); //Set pin PB4 to high
			GPIOC->ODR |= (0x0001 << 7); //Set pin PC7 to high
		}
}

void lcd_write_string(uint8_t* buffer, char* string, uint8_t line, uint8_t offset){
	for(int i =0; i < strlen(string); i++ ){
		for(int j = 0; j < 5; j++){
			memcpy(&buffer[i*6+j+line*128+offset], &character_data[string[i]-32][j], sizeof(uint8_t));
		}
	}
}

void init_lcd(uint8_t * buffer){
		lcd_init();
		memset(buffer,0x0000 , 512);
		lcd_write_string(buffer, "Score:  ",0,0);
		lcd_push_buffer(buffer);
		lcd_write_string(buffer, "Lives: ", 1, 0);
		lcd_push_buffer(buffer);
		lcd_write_string(buffer, "Level:  ", 2, 0);
		lcd_push_buffer(buffer);
		lcd_write_string(buffer, "Stage:  ", 3, 0);
		lcd_push_buffer(buffer);
}
