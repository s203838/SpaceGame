#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "timers.h"
#include "stm32f30x_conf.h" // STM32 config
#include "30010_io.h"


uint32_t drawFlag = 0;

// Setup for timer 15
void timer15Setup() {
	RCC->APB2ENR |= RCC_APB2Periph_TIM15; // Enable clock line to timer 15;

	TIM15->CR1 = 0x0000; // Configure timer 15
	TIM15->ARR = 0x002F; // Set reload value
	TIM15->PSC = 0xC3FF;// Set prescale value
	TIM15->CR1 = 0x0001; // Enable timer 15
	TIM15->DIER |= 0x0001; // Enable timer 15 interrupts
	NVIC_SetPriority(TIM1_BRK_TIM15_IRQn, 3); // Set interrupt priority
	NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn); // Enable interrupt
}

// Setup for timer 16
void timer16Setup(uint32_t reload_value){
	RCC->APB2ENR |= RCC_APB2Periph_TIM16; // Enable clock line to timer 16;

	TIM16->CR1 = 0x0000; // Configure timer 16
	TIM16->ARR = reload_value; // Set reload value
	TIM16->PSC = 0xC3FF; // Set prescale value
	TIM16->CR1 = 0x0001; // Enable timer 16
	TIM16->DIER |= 0x0001; // Enable timer 16 interrupts
	NVIC_SetPriority(TIM1_UP_TIM16_IRQn, 4); // Set interrupt priority
	NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn); // Enable interrupt
}

// Setup for timer 16
void timer17Setup(){
	RCC->APB2ENR |= RCC_APB2Periph_TIM17; // Enable clock line to timer 16;

	TIM17->CR1 = 0x0000; // Configure timer 16
	TIM17->ARR = 0x000E; // Set reload value
	TIM17->PSC = 0xA3FF; // Set prescale value
	TIM17->CR1 = 0x0001; // Enable timer 16
	TIM17->DIER |= 0x0001; // Enable timer 16 interrupts
	NVIC_SetPriority(TIM1_TRG_COM_TIM17_IRQn, 5); // Set interrupt priority
	NVIC_EnableIRQ(TIM1_TRG_COM_TIM17_IRQn); // Enable interrupt
}

// IRQ handler for timer 2 (used for buzzer)
void TIM2_IRQHandler(void){
	//timer.t2++;
	TIM2->SR &= ~0x0001;
}

// IRQ handler for timer 15
void TIM1_BRK_TIM15_IRQHandler(void){
	drawFlag |= 0x0001;
	TIM15->SR &= ~0x0001;
}

// IRQ handler for timer 16
void TIM1_UP_TIM16_IRQHandler(void){
	drawFlag |= 0x0002;
	TIM16->SR &= ~0x0001;
}
// IRQ handler for timer 16
void TIM1_TRG_COM_TIM17_IRQHandler(void){
	drawFlag |= 0x0008;
	TIM17->SR &= ~0x0001;
}



