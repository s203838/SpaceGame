#include "stm32f30x_conf.h"
#include "30010_io.h"
#include "buzzer.h"

extern uint32_t drawFlag;

void initBuzzer(){

    RCC->APB1ENR |= 0x0001;
    TIM2->CR1 = 0x0000; // Stop counter
    TIM2->ARR = 0x03E7;
    TIM2->PSC = 0x027F;
    TIM2->CR1 |= 0x0001; // Start counter

    TIM2->CCER &= ~TIM_CCER_CC3P;
    TIM2->CCER |= 0x0001 << 8;
    TIM2->CCMR2 &= ~TIM_CCMR2_OC3M;
    TIM2->CCMR2 &= ~TIM_CCMR2_CC3S;
    TIM2->CCMR2 |= TIM_OCMode_PWM1;
    TIM2->CCMR2 &= ~TIM_CCMR2_OC3PE;
    TIM2->CCMR2 |= TIM_OCPreload_Enable;
    RCC->AHBENR |=RCC_AHBENR_GPIOBEN;
    GPIOB->MODER &= ~(0x003 << (10*2)); //Clear pin B10
    GPIOB->MODER |= (0x002 << (10*2)); //Set pin B10
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_1);
}

void playFurElize(music *mus){
	uint16_t notes[10] = {659, 622, 659, 622, 659, 494, 587, 523, 440, 0};
	if(drawFlag & 0x0008){
		mus->enable = 1;
	} else if(drawFlag & 0x0008 && mus->enable && mus->on){
		setFreq(notes[mus->note]);
		mus->enable = 0;
		mus->note++;
	}
	if(mus->note > 9){
		mus->note = 0;
		mus->on = 0;
	}
}

void playSweetHome(music *mus){
	uint16_t notes[33] = {294, 0, 294, 0, 587, 440, 0, 294, 261, 0, 261, 0, 587, 392, 292, 0, 196, 0, 196, 392, 392, 392, 0, 392, 220, 247, 294, 330, 294, 247, 440, 392, 0};
	if(getTimer16() % 4 != 0){
		mus->enable = 1;
	} else if(getTimer15() % 3 == 0 && mus->enable && mus->on){
		setFreq(notes[mus->note]);
		mus->enable = 0;
		mus->note++;
	}
	if(mus->note > 32){
		mus->note = 0;
		mus->on = 0;
	}
}

void setFreq(uint16_t freq){  //Sætter freq
	uint32_t reload = 64e6 / freq / (0x0280) - 1;
    TIM2->ARR = reload;
    TIM2->CCR3 = reload/2;
    TIM2->EGR |= 1;
}


