#include "draw.h"

void drawPlayer(Player * player) {
	// Checks if its time to draw
	if(drawFlag & 0x0001) {
			// Draws the player object
			gotoxy(player->posY, player->posX);
			printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
			printf("%c[%d%c", ESC, 1, 'B');
			printf("%c[%d%c", ESC, 7, 'D');
			printf("%c%c%c%c%c%c%c", 32, 179, 219, 219, 92, 32, 32);
			printf("%c[%d%c", ESC, 1, 'B');
			printf("%c[%d%c", ESC, 7, 'D');
			printf("%c%c%c%c%c%c%c", 32, 179, 219, 219, 219, 92, 32);
			printf("%c[%d%c", ESC, 1, 'B');
			printf("%c[%d%c", ESC, 7, 'D');
			printf("%c%c%c%c%c%c%c", 32, 179, 219, 219, 219, 219, 32);
			printf("%c[%d%c", ESC, 1, 'B');
			printf("%c[%d%c", ESC, 7, 'D');
			printf("%c%c%c%c%c%c%c", 32, 179, 219, 219, 219, 47, 32);
			printf("%c[%d%c", ESC, 1, 'B');
			printf("%c[%d%c", ESC, 7, 'D');
			printf("%c%c%c%c%c%c%c", 32, 179, 219, 219, 47, 32, 32);


			//Removes earlier player drawing above player
			if(player->velY > 0){
				for(uint8_t i = 1; i <= player->velY; i++){
					gotoxy(player->posY + 1 - i, player->posX - 1);
					printf("%c%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32, 32);
				}
			}
			//Removes earlier player drawing below player
			if(player->velY < 0){
				for(int8_t i = -1; i >= player->velY; i--){
					gotoxy(player->posY + 4 - i, player->posX - 2);
					printf("%c%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32, 32);
				}
			}
			//Removes earlier player drawing left of player
			if (player->velX > 0){
				for(uint8_t i = 1; i <= player->velX; i++){
					for(uint8_t j = 0; j < 5; j++){
					gotoxy(player->posY + 1 + j, player->posX - i);
						printf("%c", 32);
					}
				}
			}
			//Removes earlier player drawing right of player
			if (player->velX < 0){
				for(int8_t i = -1; i >= player->velX; i--){
					for(uint8_t j = 0; j < 5; j++){
						gotoxy(player->posY + j, player->posX + 4 - i);
						printf("%c", 32);
					}
				}
			}
			// Moves the player based on velocity if the player is within the give space
			if (player->posX + player->velX >= 1 && player->posX + player->velX <= 280)
				player->posX += player->velX;
			if (player->posY + player->velY >= 1 && player->posY + player->velY <= 245)
				player->posY += player->velY;
	}
}

// Draws asteroid at position
void drawAstroid(AstroidNode* astroid) {
	gotoxy(astroid->posY, astroid->posX);
	printf("%c%c%c%c%c%c%c%c", 32, 32, 95, 95, 95, 95, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 47, 167, 32, 32, 32, 92, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c%c", 40, 32, 32, 32, 32, 167, 32, 41, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 9, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 92, 32, 32, 32, 32, 47, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 92, 167, 32, 47, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 92, 47, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 8, 'D');
	printf("%c%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32, 32);

	// Moves the asteroid by its velocity;
	astroid->posX += astroid->velX;
	astroid->posY += astroid->velY;
}

// Draws bullet at position
void drawBullet(BulletNode* bullet) {
		gotoxy(bullet->posY - bullet->velY, bullet->posX - bullet->velX);
		printf("   ");
		gotoxy(bullet->posY, bullet->posX);
		printf("o");
		bullet->posX += bullet->velX;
		bullet->posY += bullet->velY;
}

// Draws dockingStation at position
void drawDockingStation(DockingStation * ds) {
	gotoxy(ds->posY ,ds->posX);
	printf("%c[38;5;33m", ESC);
	printf("%c%c%c%c%c%c%c", 223, 223, 223, 223, 223, 223, 223);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 221, 32, 32, 32, 32, 32, 222);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 221, 32, 32, 32, 32, 32, 222);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 221, 32, 32, 32, 32, 32, 222);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 221, 32, 32, 32, 32, 32, 222);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 221, 32, 32, 32, 32, 32, 222);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 220, 220, 220, 220, 220, 220, 220);
	printf("%c[0m", ESC);
}

void unDrawDocking(DockingStation ds) {
	gotoxy(ds.posY ,ds.posX);
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[%d%c", ESC, 1, 'B');
	printf("%c[%d%c", ESC, 7, 'D');
	printf("%c%c%c%c%c%c%c", 32, 32, 32, 32, 32, 32, 32);
	printf("%c[0m", ESC);
}
